<?php

namespace BillatAddons;

/**
 * Integrate with Elementor.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

class Elementor {
	/**
	 * Instance
	 *
	 * @access private
	 */
	private static $_instance = null;


	/**
	 * Instance
	 *
	 * Ensures only one instance of the class is loaded or can be loaded.
	 *
	 * @return Billat_Addons_Elementor An instance of the class.
	 */
	public static function instance() {
		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	/**
	 * Constructor
	 */
	public function __construct() {
		spl_autoload_register( [ $this, 'autoload' ] );

		$this->_includes();
		$this->add_actions();
	}

	/**
	 * Auto load widgets
	 */
	public function autoload( $class ) {
		if ( 0 !== strpos( $class, __NAMESPACE__ ) ) {
			return;
		}

		if ( false === strpos( $class, 'Widgets' ) ) {
			return;
		}

		$path     = explode( '\\', $class );
		$filename = strtolower( array_pop( $path ) );

		$folder = array_pop( $path );

		if ( ! in_array( $folder, array( 'Widgets' ) ) ) {
			return;
		}

		$filename = str_replace( '_', '-', $filename );
		$filename = BILLAT_ADDONS_DIR . 'inc/elementor/widgets/' . $filename . '.php';

		if ( is_readable( $filename ) ) {
			include( $filename );
		}
	}

	/**
	 * Includes files which are not widgets
	 */
	private function _includes() {
		
	}

	/**
	 * Hooks to init
	 */
	protected function add_actions() {
		add_action( 'elementor/frontend/after_enqueue_styles', [ $this, 'styles' ] );
		add_action( 'elementor/frontend/after_register_scripts', [ $this, 'scripts' ] );


		add_action( 'elementor/controls/controls_registered', [ $this, 'init_controls' ] );
		add_action( 'elementor/widgets/widgets_registered', [ $this, 'init_widgets' ] );

		add_action( 'elementor/elements/categories_registered', [ $this, 'add_category' ] );

		add_action( 'elementor/editor/after_enqueue_styles', [ $this, 'editor_styles' ] );
	}

	/**
	 * Register styles
	 */
	public function styles() {
	}

	/**
	 * Register styles
	 */
	public function scripts() {
		wp_register_script( 'billat-elementor', BILLAT_ADDONS_URL . '/assets/js/elementor.js', array( 'jquery' ), '20170530', true );
	}


	/**
	 * Register styles
	 */
	public function editor_styles() {
		wp_enqueue_style( 'billaticons', BILLAT_ADDONS_URL . 'assets/css/billaticons.css', array(), '1.0.0' );
	}

	/**
	 * Init Controls
	 */
	public function init_controls( $controls_registry ) {
		add_action( 'elementor/icons_manager/additional_tabs', [ $this, 'elementor_custom_icons' ] );

	}

	/**
	 * Init Widgets
	 */
	public function init_widgets() {
		$widgets_manager = \Elementor\Plugin::instance()->widgets_manager;

		$widgets_manager->register_widget_type( new \BillatAddons\Elementor\Widgets\Portfolio_Meta() );
		// $widgets_manager->register_widget_type( new \BillatAddons\Elementor\Widgets\Countdown() );
		// $widgets_manager->register_widget_type( new \BillatAddons\Elementor\Widgets\Newsletter() );
		// $widgets_manager->register_widget_type( new \BillatAddons\Elementor\Widgets\Counter() );
		// $widgets_manager->register_widget_type( new \BillatAddons\Elementor\Widgets\Brands() );
		// $widgets_manager->register_widget_type( new \BillatAddons\Elementor\Widgets\Member() );
		// $widgets_manager->register_widget_type( new \BillatAddons\Elementor\Widgets\FAQs() );
		// $widgets_manager->register_widget_type( new \BillatAddons\Elementor\Widgets\Icon_Box() );
		// $widgets_manager->register_widget_type( new \BillatAddons\Elementor\Widgets\Testimonials() );
		// $widgets_manager->register_widget_type( new \BillatAddons\Elementor\Widgets\Image_Grid() );
		// $widgets_manager->register_widget_type( new \BillatAddons\Elementor\Widgets\Image_Masonry() );
		// $widgets_manager->register_widget_type( new \BillatAddons\Elementor\Widgets\Image_Slides() );
		// $widgets_manager->register_widget_type( new \BillatAddons\Elementor\Widgets\Video() );
		// $widgets_manager->register_widget_type( new \BillatAddons\Elementor\Widgets\Slides() );
		// $widgets_manager->register_widget_type( new \BillatAddons\Elementor\Widgets\Icon_List() );
	}

	/**
	 * Add Billat category
	 */
	public function add_category( $elements_manager ) {
		$elements_manager->add_category(
			'billat',
			[
				'title' => esc_html__( 'Billat', 'billat' )
			]
		);
	}


	public function elementor_custom_icons( $additional_tabs ) {
		$additional_tabs['billaticons'] = [
			'name'          => 'billaticons',
			'label'         => esc_html__( 'Billaticons', 'billat' ),
			'url'           => self::get_asset_url( 'billaticons' ),
			'enqueue'       => [ self::get_asset_url( 'billaticons' ) ],
			'prefix'        => 'icon-',
			'displayPrefix' => 'icon',
			'labelIcon'     => 'icon-pencil4',
			'ver'           => '1.0.0',
			'fetchJson'     => self::get_asset_url( 'billaticons', 'js', false ),
			'native'        => true,
		];

		return $additional_tabs;
	}

	public static function get_asset_url( $filename, $ext_type = 'css', $add_suffix = true ) {

		$url = BILLAT_ADDONS_URL . 'assets/' . $ext_type . '/' . $filename;

		if ( $add_suffix ) {
			$url .= '.min';
		}

		return $url . '.' . $ext_type;
	}

	/**
	 * Retrieve the list of taxonomy
	 *
	 * @return array Widget categories.
	 */
	public static function get_taxonomy( $taxonomy = 'product_cat' ) {

		$output = array();

		$categories = get_categories(
			array(
				'taxonomy' => $taxonomy
			)
		);

		foreach ( $categories as $category ) {
			$output[$category->slug] = $category->name;
		}

		return $output;
	}

	/**
	 * @param array $settings
	 *
	 * @return array Slick Options
	 */
	public static function get_data_slick( $settings = array() ) {
		$show_dots   = ( in_array( $settings['navigation'], [ 'dots', 'both' ] ) );
		$show_arrows = ( in_array( $settings['navigation'], [ 'arrows', 'both' ] ) );

		$show_tablet_dots   = ( in_array( $settings['navigation_tablet'], [ 'dots', 'both' ] ) );
		$show_tablet_arrows = ( in_array( $settings['navigation_tablet'], [ 'arrows', 'both' ] ) );
		$tab_show           = $settings['slidesToShow_tablet'] ? absint( $settings['slidesToShow_tablet'] ) : 1;
		$tab_scroll         = $settings['slidesToScroll_tablet'] ? absint( $settings['slidesToScroll_tablet'] ) : 1;

		if ( isset( $settings['rows'] ) ):
			$tab_rows = $settings['rows_tablet'] ? absint( $settings['rows_tablet'] ) : 1;
			$mob_rows = $settings['rows_mobile'] ? absint( $settings['rows_mobile'] ) : 1;
		else:
			$tab_rows = $mob_rows = $settings['rows'] = 1;
		endif;

		$mob_show           = $settings['slidesToShow_mobile'] ? absint( $settings['slidesToShow_mobile'] ) : 1;
		$mob_scroll         = $settings['slidesToScroll_mobile'] ? absint( $settings['slidesToScroll_mobile'] ) : 1;
		$show_mobile_dots   = ( in_array( $settings['navigation_mobile'], [ 'dots', 'both' ] ) );
		$show_mobile_arrows = ( in_array( $settings['navigation_mobile'], [ 'arrows', 'both' ] ) );

		$is_rtl = is_rtl();

		$carousel_settings = [
			'arrows'         => $show_arrows,
			'dots'           => $show_dots,
			'autoplay'       => ( 'yes' === $settings['autoplay'] ),
			'infinite'       => ( 'yes' === $settings['infinite'] ),
			'autoplaySpeed'  => absint( $settings['autoplay_speed'] ),
			'speed'          => absint( $settings['speed'] ),
			'slidesToShow'   => absint( $settings['slidesToShow'] ),
			'slidesToScroll' => absint( $settings['slidesToScroll'] ),
			'rows'           => absint( $settings['rows'] ),
			'rtl'            => $is_rtl,
			'responsive'     => array(
				array(
					'breakpoint' => 1025,
					'settings'   => array(
						'arrows'         => $show_tablet_arrows,
						'dots'           => $show_tablet_dots,
						'slidesToShow'   => $tab_show,
						'slidesToScroll' => $tab_scroll,
						'rows'           => $tab_rows,
					)
				),
				array(
					'breakpoint' => 768,
					'settings'   => array(
						'arrows'         => $show_mobile_arrows,
						'dots'           => $show_mobile_dots,
						'slidesToShow'   => $mob_show,
						'slidesToScroll' => $mob_scroll,
						'rows'           => $mob_rows,
					)
				)
			)
		];

		return $carousel_settings;
	}
}

Elementor::instance();