<?php

namespace BillatAddons\Elementor\Widgets;

use Elementor\Controls_Manager;
use Elementor\Group_Control_Typography;
use Elementor\Widget_Base;

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Icon Box widget
 */
class Portfolio_Meta extends Widget_Base {
	/**
	 * Retrieve the widget name.
	 *
	 * @return string Widget name.
	 */
	public function get_name() {
		return 'billat-portfolio-meta';
	}

	/**
	 * Retrieve the widget title.
	 *
	 * @return string Widget title.
	 */
	public function get_title() {
		return esc_html__( 'Billat - Portfolio Meta', 'billat' );
	}

	/**
	 * Retrieve the widget icon.
	 *
	 * @return string Widget icon.
	 */
	public function get_icon() {
		return 'eicon-editor-link';
	}

	/**
	 * Retrieve the list of categories the widget belongs to.
	 *
	 * @return array Widget categories.
	 */
	public function get_categories() {
		return [ 'billat' ];
	}

	/**
	 * Register the widget controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_controls() {
		$this->_register_portfolio_meta_settings_controls();
	}

	/**
	 * Register the widget content controls.
	 *
	 * Adds different input fields to allow the user to change and customize the widget settings.
	 *
	 * @access protected
	 */
	protected function _register_portfolio_meta_settings_controls() {
		// Settings
		$this->start_controls_section(
			'section_portfolio_meta',
			[ 'label' => esc_html__( 'Portfolio Meta', 'billat' ) ]
		);

		$repeater = new \Elementor\Repeater();
		$repeater->add_control(
			'label', [
				'label'       => esc_html__( 'Label', 'billat' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
			]
		);
		$repeater->add_control(
			'value', [
				'label'       => esc_html__( 'Value', 'billat' ),
				'type'        => Controls_Manager::TEXT,
				'default'     => '',
				'label_block' => true,
			]
		);

		$this->add_control(
			'portfolio_meta',
			[
				'label'         => esc_html__( 'Fields', 'billat' ),
				'type'          => Controls_Manager::REPEATER,
				'fields'        => $repeater->get_controls(),
				'default'       => [
					[
						'label' => esc_html__( 'Value:', 'billat' ),
						'value' => esc_html__( '25,000,000 $', 'billat' ),
					],
					[
						'label' => esc_html__( 'Year:', 'billat' ),
						'value' => esc_html__( '2019', 'billat' ),
					],
					[
						'label' => esc_html__( 'Project Status:', 'billat' ),
						'value' => esc_html__( 'Complete', 'billat' ),
					],
					[
						'label' => esc_html__( 'Client:', 'billat' ),
						'value' => esc_html__( 'Naval Facilities', 'billat' ),
					],
				],
				'title_field'   => '{{{ label }}}',
				'prevent_empty' => false
			]
		);

		$this->end_controls_section(); // End Heading Settings

		// Style
		$this->start_controls_section(
			'section_Portfolio Meta_style',
			[
				'label' => esc_html__( 'Portfolio Meta', 'billat' ),
				'tab'   => Controls_Manager::TAB_STYLE,
			]
		);
		$this->add_responsive_control(
			'left_spacing',
			[
				'label'     => __( 'Left Spacing', 'billat' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 200,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .billat-portfolio-meta ul' => 'padding-left: {{SIZE}}{{UNIT}}',
				],
			]
		);
		$this->add_control(
			'border_left_style',
			[
				'label'        => __( 'Border Left Style', 'billat' ),
				'type'         => Controls_Manager::POPOVER_TOGGLE,
				'label_off'    => __( 'Default', 'billat' ),
				'label_on'     => __( 'Custom', 'billat' ),
				'return_value' => 'yes',
			]
		);

		$this->start_popover();

		$this->add_control(
			'border_color',
			[
				'label'     => esc_html__( 'Border Color', 'billat' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .billat-portfolio-meta ul' => 'border-left-color: {{VALUE}}',
				],
			]
		);

		$this->add_responsive_control(
			'border_width',
			[
				'label'     => __( 'Border Width', 'billat' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 50,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .billat-portfolio-meta ul' => 'border-left-width: {{SIZE}}{{UNIT}}',
				],
			]
		);

		$this->end_popover();

		$this->add_control(
			'divider_1',
			[
				'type' => Controls_Manager::DIVIDER,
			]
		);

		// Item Spacing
		$this->add_responsive_control(
			'item_spacing',
			[
				'label'     => __( 'Item Spacing', 'billat' ),
				'type'      => Controls_Manager::SLIDER,
				'range'     => [
					'px' => [
						'max' => 100,
						'min' => 0,
					],
				],
				'selectors' => [
					'{{WRAPPER}} .billat-portfolio-meta li' => 'margin-bottom: {{SIZE}}{{UNIT}}',
					'{{WRAPPER}} .billat-portfolio-meta li:last-child' => 'margin-bottom: 0',
				],
			]
		);

		// Tabs

		$this->start_controls_tabs(
			'meta_style_tabs'
		);

		$this->start_controls_tab(
			'label_style_tab',
			[
				'label' => __( 'Label', 'billat' ),
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'label_typography',
				'selector' => '{{WRAPPER}} .billat-portfolio-meta li .meta-label',
			]
		);

		$this->add_control(
			'label_color',
			[
				'label'     => esc_html__( 'Color', 'billat' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .billat-portfolio-meta li .meta-label' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->start_controls_tab(
			'value_style_tab',
			[
				'label' => __( 'Value', 'billat' ),
			]
		);

		$this->add_group_control(
			Group_Control_Typography::get_type(),
			[
				'name'     => 'value_typography',
				'selector' => '{{WRAPPER}} .billat-portfolio-meta li .meta-value',
			]
		);

		$this->add_control(
			'value_color',
			[
				'label'     => esc_html__( 'Color', 'billat' ),
				'type'      => Controls_Manager::COLOR,
				'selectors' => [
					'{{WRAPPER}} .billat-portfolio-meta li .meta-value' => 'color: {{VALUE}};',
				],
			]
		);

		$this->end_controls_tab();

		$this->end_controls_tabs();

		$this->end_controls_section();
	}

	/**
	 * Render icon box widget output on the frontend.
	 *
	 * Written in PHP and used to generate the final HTML.
	 */
	protected function render() {
		$settings = $this->get_settings_for_display();

		$this->add_render_attribute(
			'wrapper', 'class', [
				'billat-portfolio-meta',
			]
		);

		$links = $settings['portfolio_meta'];

		?>
		<div <?php echo $this->get_render_attribute_string( 'wrapper' ); ?>>
			<?php if ( ! empty( $links ) ) : ?>
				<ul>
					<?php
					foreach ( $links as $index => $item ) {
						$link_key = 'meta_' . $index;

						echo sprintf( 
							'<li>
								<span class="meta-label">%s</span>
								<span class="meta-value">%s</span>
							</li>',
							$item['label'],
							$item['value']
						);
					}
					?>
				</ul>
			<?php endif; ?>
		</div>
		<?php
	}

	/**
	 * Render icon box widget output in the editor.
	 *
	 * Written as a Backbone JavaScript template and used to generate the live preview.
	 */
	protected function _content_template() {
	}

	/**
	 * Get the link control
	 *
	 * @return string.
	 */
	protected function get_link_control( $link_key, $url, $content, $attr = [ ] ) {
		$attr_default = [
			'href' => $url['url'] ? $url['url'] : '#',
		];

		if ( $url['is_external'] ) {
			$attr_default['target'] = '_blank';
		}

		if ( $url['nofollow'] ) {
			$attr_default['rel'] = 'nofollow';
		}

		$attr = wp_parse_args( $attr, $attr_default );

		$this->add_render_attribute( $link_key, $attr );

		return sprintf( '<a %1$s>%2$s</a>', $this->get_render_attribute_string( $link_key ), $content );
	}
}