<?php
/**
 * Hooks for importer
 *
 * @package BillatCore
 */


/**
 * Importer the demo content
 *
 * @since  1.0
 *
 */
function billat_vc_addons_importer() {
	return array(
		array(
			'name'       => 'Home Default',
			'preview'    => 'http://demo3.drfuri.com/soo-importer/billat/home-default/preview.jpg',
			'content'    => 'http://demo3.drfuri.com/soo-importer/billat/home-default/demo-content.xml',
			'customizer' => 'http://demo3.drfuri.com/soo-importer/billat/home-default/customizer.dat',
			'widgets'    => 'http://demo3.drfuri.com/soo-importer/billat/home-default/widgets.wie',
			'sliders'    => 'http://demo3.drfuri.com/soo-importer/billat/home-default/sliders.zip',
			'pages'      => array(
				'front_page' => 'Home Default',
				'blog'       => 'Blog',
				'shop'       => 'Shop',
				'cart'       => 'Cart',
				'checkout'   => 'Checkout',
				'my_account' => 'My Account',
			),
			'menus'      => array(
				'primary' => 'primary-menu',
			),
			'options'    => array(
				'shop_catalog_image_size'   => array(
					'width'  => 400,
					'height' => 400,
					'crop'   => 1,
				),
				'shop_single_image_size'    => array(
					'width'  => 600,
					'height' => 600,
					'crop'   => 1,
				),
				'shop_thumbnail_image_size' => array(
					'width'  => 70,
					'height' => 70,
					'crop'   => 1,
				),
			),
		),
	);
}

add_filter( 'soo_demo_packages', 'billat_vc_addons_importer', 20 );
