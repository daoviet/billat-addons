<?php
/**
 * Load and register widgets
 *
 * @package Billat
 */

require_once BILLAT_ADDONS_DIR . '/inc/widgets/service-menu.php';

/**
 * Register widgets
 *
 * @since  1.0
 *
 * @return void
 */
function billat_register_widgets() {
	if ( class_exists( 'WC_Widget' ) ) {
		// require_once BILLAT_ADDONS_DIR . '/inc/widgets/product-categories.php';

		// register_widget( 'Billat_Widget_Product_Categories' );
	}

	register_widget( 'Billat_Services_Menu_Widget' );
}

add_action( 'widgets_init', 'billat_register_widgets', 100 );

function billat_widget_archive_count( $output ) {
	$output = preg_replace( '|\((\d+)\)|', '<span class="count">(\\1)</span>', $output );

	return $output;
}

add_filter( 'wp_list_categories', 'billat_widget_archive_count' );
add_filter( 'get_archives_link', 'billat_widget_archive_count' );