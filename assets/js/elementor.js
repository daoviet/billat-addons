(function ( $ ) {
	'use strict';

	/**
	 * LazyLoad
	 */
	var lazyLoadHandler = function ( $els ) {
		if ( $els.length === 0 ) {
			$els = $( 'body' );
		}
		$els.find( 'img.lazy' ).lazyload( {
			load: function () {
				$( this ).removeClass( 'lazy' );
			}
		} );
	};

	var slideCarousel = function ( $scope, $ ) {
		$scope.find( '.billat-slides-wrapper' ).each( function () {
			var $selector = $( this ).find( '.billat-slides' ),
				elementSettings = $selector.data( 'slider_options' ),
				$arrow_wrapper = $( this ).find( '.arrows-inner' ),
				slidesToShow = parseInt( elementSettings.slidesToShow );

			$selector.not( '.slick-initialized' ).slick( {
				rtl          : $( 'body' ).hasClass( 'rtl' ),
				slidesToShow : slidesToShow,
				arrows       : elementSettings.arrows,
				appendArrows : $arrow_wrapper,
				dots         : elementSettings.dots,
				infinite     : elementSettings.infinite,
				prevArrow    : '<span class="slick-prev-arrow"><i class=""></i></span>',
				nextArrow    : '<span class="slick-next-arrow"><i class=""></i></span>',
				autoplay     : elementSettings.autoplay,
				autoplaySpeed: parseInt( elementSettings.autoplaySpeed ),
				speed        : parseInt( elementSettings.speed ),
				fade         : elementSettings.fade,
				pauseOnHover : elementSettings.pauseOnHover,
				responsive   : []
			} );

			var animation = $selector.data( 'animation' );

			if ( animation ) {
				$selector
					.on( 'beforeChange', function () {
						var $sliderContent = $selector.find( '.billat-slide-content' ),
							$sliderPriceBox = $selector.find( '.billat-slide-price-box' );

						$sliderContent.removeClass( 'animated' + ' ' + animation ).hide();

						$sliderPriceBox.removeClass( 'animated zoomIn' ).hide();
					} )
					.on( 'afterChange', function ( event, slick, currentSlide ) {
						var $currentSlide = $( slick.$slides.get( currentSlide ) ).find( '.billat-slide-content' ),
							$currentPriceBox = $( slick.$slides.get( currentSlide ) ).find( '.billat-slide-price-box' );

						$currentSlide.show().addClass( 'animated' + ' ' + animation );

						$currentPriceBox.show().addClass( 'animated zoomIn' );
					} );
			}

			$selector.on( 'afterChange', function () {
				lazyLoadHandler( $selector );
			} );
		} );
	};

	/**
	 * Elementor JS Hooks
	 */
	$( window ).on( "elementor/frontend/init", function () {
		// Slider
		elementorFrontend.hooks.addAction(
			"frontend/element_ready/billat-slides.default",
			slideCarousel
		);
	} );
})
( jQuery );