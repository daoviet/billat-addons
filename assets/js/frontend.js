(function ( $ ) {
	'use strict';

	var billat = billat || {};

	billat.init = function () {
		billat.$body = $( document.body ),
			billat.$window = $( window ),
			billat.$header = $( '#masthead' );
		
	};

	
	/**
	 * Document ready
	 */
	$( function () {
		billat.init();
	} );

})( jQuery );