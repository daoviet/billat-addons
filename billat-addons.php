<?php
/**
 * Plugin Name: Billat Addons
 * Plugin URI: http://davi.com/plugins/billat-addons.zip
 * Description: Extra elements for Elementor. It was built for Billat theme.
 * Version: 1.0.0
 * Author: Davi
 * Author URI: http://davi.com/
 * License: GPL2+
 * Text Domain: billat
 * Domain Path: /lang/
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! defined( 'BILLAT_ADDONS_DIR' ) ) {
	define( 'BILLAT_ADDONS_DIR', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'BILLAT_ADDONS_URL' ) ) {
	define( 'BILLAT_ADDONS_URL', plugin_dir_url( __FILE__ ) );
}

require_once BILLAT_ADDONS_DIR . '/inc/backend/portfolio.php';
require_once BILLAT_ADDONS_DIR . '/inc/backend/services.php';
require_once BILLAT_ADDONS_DIR . '/inc/backend/socials.php';

require_once BILLAT_ADDONS_DIR . '/inc/widgets/widgets.php';

if ( is_admin() ) {
	require_once BILLAT_ADDONS_DIR . '/inc/backend/importer.php';
}

/**
 * Init
 */
function billat_vc_addons_init() {
	load_plugin_textdomain( 'billat', false, dirname( plugin_basename( __FILE__ ) ) . '/lang' );

	new BillatCore_Portfolio;
	new BillatCore_Services;
}

add_action( 'after_setup_theme', 'billat_vc_addons_init', 20 );

/**
 * Undocumented function
 */
function billat_init_elementor() {
	// Check if Elementor installed and activated
	if ( ! did_action( 'elementor/loaded' ) ) {
		return;
	}

	// Check for required Elementor version
	if ( ! version_compare( ELEMENTOR_VERSION, '2.0.0', '>=' ) ) {
		return;
	}

	// Check for required PHP version
	if ( version_compare( PHP_VERSION, '5.4', '<' ) ) {
		return;
	}

	// Once we get here, We have passed all validation checks so we can safely include our plugin
	include_once( BILLAT_ADDONS_DIR . 'inc/elementor/elementor.php' );
}

add_action( 'plugins_loaded', 'billat_init_elementor' );

/**
 * Check plugin dependencies.
 * Check if page builder plugin is installed.
 */
function billat_check_dependencies() {
	if ( ! defined( 'ELEMENTOR_VERSION' ) ) {
		$plugin_data = get_plugin_data( __FILE__ );

		printf(
			'<div class="notice notice-warning is-dismissible"><p>%s</p></div>',
			sprintf(
				__( '<strong>%s</strong> requires <strong><a href="https://wordpress.org/plugins/elementor/" target="_blank">Elementor Page Builder</a></strong> plugin to be installed and activated on your site.', 'billat' ),
				$plugin_data['Name']
			)
		);
	}
}

add_action( 'admin_notices', 'billat_check_dependencies' );
